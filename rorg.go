package esp3

import "errors"

// RORGType describes ESP3 telegram data type
type RORGType int

const (
	// RORGType4BS is a ESP3 4BS RadioPacket
	RORGType4BS = RORGType(0xa5)
	// RORGTypeADT is a ESP3 ADT RadioPacket
	RORGTypeADT = RORGType(0xa6)
	// RORGTypeVLD is a ESP3 VLD RadioPacket
	RORGTypeVLD = RORGType(0xd2)
	// RORGType1BS is a ESP3 1BS RadioPacket
	RORGType1BS = RORGType(0xd5)
	// RORGTypeRPS is a ESP3 RPS RadioPacket
	RORGTypeRPS = RORGType(0xf6)
)

const (
	// T21PTM1XX indicates T21 mode PTM-1xx
	T21PTM1XX = 0
	// T21PTM2XX indicates T21 mode PTM-2xx
	T21PTM2XX = 1
	// T21Unknown indicates T21 bit isn't used
	T21Unknown = 255
)

const (
	// NUUnassigned means NU-bit was 0 (U-message)
	NUUnassigned = 0
	// NUNormal meas NU-bit was 1 (N-message)
	NUNormal = 1
	// NUUnknown means NU-bit was not set or isn't used.
	NUUnknown = 255
)

func parseRPSData(input []byte, res *RadioPacket) error {
	if len(input) != 6 {
		return errors.New("Invalid data size for RPS")
	}

	res.Data = input[0:1]
	res.SenderID = uint32(input[1]) << 24
	res.SenderID |= uint32(input[2]) << 16
	res.SenderID |= uint32(input[3]) << 8
	res.SenderID |= uint32(input[4])
	res.RC = input[5] & 0x0f
	res.NU = (input[5] >> 4) & 1
	res.T21 = (input[5] >> 5) & 1

	if res.NU > 1 {
		res.NU = NUUnknown
	}

	if res.T21 > 1 {
		res.T21 = T21Unknown
	}

	return nil
}

func parse1BSData(input []byte, res *RadioPacket) error {
	if len(input) != 6 {
		return errors.New("Invalid data size for 1BS")
	}

	res.Data = input[0:1]
	res.SenderID = uint32(input[1]) << 24
	res.SenderID |= uint32(input[2]) << 16
	res.SenderID |= uint32(input[3]) << 8
	res.SenderID |= uint32(input[4])
	res.RC = input[5] & 0x0f
	res.NU = NUUnknown
	res.T21 = T21Unknown

	return nil
}

func parse4BSData(input []byte, res *RadioPacket) error {
	if len(input) != 9 {
		return errors.New("Invalid data size for 4BS")
	}

	res.Data = input[0:4]
	res.SenderID = uint32(input[4]) << 24
	res.SenderID |= uint32(input[5]) << 16
	res.SenderID |= uint32(input[6]) << 8
	res.SenderID |= uint32(input[7])
	res.RC = input[8] & 0x0f
	res.NU = NUUnknown
	res.T21 = T21Unknown

	return nil
}

func parseADTData(input []byte, res *RadioPacket) error {

	return errors.New("ADT parser not implemented")
}

func parseVLDData(input []byte, res *RadioPacket) error {
	ilen := len(input)
	if ilen < 7 || ilen > 20 {
		return errors.New("Invalid data size for VLD")
	}

	vlen := ilen - 5

	res.Data = input[0:vlen]
	res.SenderID = uint32(input[vlen+0]) << 24
	res.SenderID |= uint32(input[vlen+1]) << 16
	res.SenderID |= uint32(input[vlen+2]) << 8
	res.SenderID |= uint32(input[vlen+3])
	res.RC = input[vlen+4] & 0x0f
	res.NU = NUUnknown
	res.T21 = T21Unknown

	// CRC byte not handled.

	return nil
}
