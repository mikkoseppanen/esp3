package esp3

import "errors"

const (
	// DestinationBroadcast is the contant Destination ID that identifies messages
	// that were broadcasted or not intended for any specific recipient.
	DestinationBroadcast = 0xFFFFFFFF
)

// PacketType describes the ESP3 packet data type
type PacketType int

const (
	// PacketTypeUnknown is used when packet can't be identified or is empty.
	PacketTypeUnknown = PacketType(0)
	// PacketTypeRadio identifies a Radio packet
	PacketTypeRadio = PacketType(1)
	// PacketTypeEvent identifies a Event packet
	PacketTypeEvent = PacketType(4)
)

// PacketHeader describes the header fields of a single Packet
type PacketHeader struct {
	Type               PacketType
	DataLength         int
	OptionalDataLength int
	CRC                int
}

// RawPacket describes the fields of a single ESP3 packet
type RawPacket struct {
	Header       PacketHeader
	Data         []byte
	OptionalData []byte
	CRC          int
}

// RadioPacket defines fields of a single ESP3 Radio packet
type RadioPacket struct {
	Data          []byte
	SubTelNum     uint8
	DestinationID uint32
	SenderID      uint32
	RSSI          uint8
	Encryption    uint8
	RORG          RORGType
	RC            uint8
	NU            uint8
	T21           uint8
}

// AsRadioPacket returns the RawPacket data parsed as a RadioPacket.
// If RawPacket has wrong type or invalid data, error is returned.
func (r *RawPacket) AsRadioPacket() (*RadioPacket, error) {
	if r.Header.Type != PacketTypeRadio {
		return nil, errors.New("Can't parse type %d as PacketTypeRadio")
	}

	if r.Header.OptionalDataLength != 7 || r.Header.OptionalDataLength != len(r.OptionalData) {
		return nil, errors.New("Optional data size invalid for PacketTypeRadio")
	}

	if len(r.Data) < 6 {
		return nil, errors.New("Data too short")
	}

	res := &RadioPacket{Data: make([]byte, 0)}
	res.SubTelNum = uint8(r.OptionalData[0])
	res.DestinationID = uint32(r.OptionalData[1])
	res.DestinationID |= uint32(r.OptionalData[2]) << 8
	res.DestinationID |= uint32(r.OptionalData[3]) << 16
	res.DestinationID |= uint32(r.OptionalData[4]) << 24
	res.RSSI = uint8(r.OptionalData[5])
	res.Encryption = uint8(r.OptionalData[6])
	res.RORG = RORGType(r.Data[0])

	switch res.RORG {
	case RORGTypeRPS:
		if err := parseRPSData(r.Data[1:], res); err != nil {
			return nil, err
		}

		break

	case RORGType1BS:
		if err := parse1BSData(r.Data[1:], res); err != nil {
			return nil, err
		}

		break

	case RORGType4BS:
		if err := parse4BSData(r.Data[1:], res); err != nil {
			return nil, err
		}

		break

	case RORGTypeADT:
		if err := parseADTData(r.Data[1:], res); err != nil {
			return nil, err
		}

		break

	case RORGTypeVLD:
		if err := parseVLDData(r.Data[1:], res); err != nil {
			return nil, err
		}

		break
	}

	return res, nil
}
