# ecexpod

A simple sample of usage of the ESP3 package.

## Running the server

Run `ecexpod -help` for usage.

## API

### Reading device data

```
GET /api/device/
```

Returns a list of devices available and their last known states

```
GET /api/device/{senderid}
```

Returns details of the device identified by `{senderid}`

### Managing subscriptions

```
GET /api/subscription/
```
Returns list of currently active subscribers.

```
POST /api/subscription/

{
  "subscriptions": [senderid, senderid...],
  "callback": "callback URL"
}
```
Registers a new subscriber. On success, 201 CREATED is returned and a  Location
header is set to point to the new subscribers details.

```
GET /api/subscription/{key}
```
Returns details of a subscriber identified by `{key}`

```
DELETE /api/subscription/{key}
```
Removes the subscriber identified by `{key}`

## subscriptions

Once a subscription is done, the subscribers callback URL is called once per
every change in the devices listed in subscribers subscription list.
The callback should expect a POST request with content-type `application/json`
and a body containing a JSON-document describing a single device. Format for the
device JSON is the same as is received from `/api/device/{senderid}`.
