package main

import (
	"encoding/binary"
	"encoding/json"
	"log"
	"path"
	"time"

	"github.com/boltdb/bolt"
)

// BucketConfigDevices is the bucket name for device configurations
var BucketConfigDevices = []byte("devices")

// BucketConfigSubs is the bucket name for subscriptions
var BucketConfigSubs = []byte("subscriptions")

// itob returns an 8-byte big endian representation of v.
func itob(v uint64) []byte {
	b := make([]byte, 8)
	binary.BigEndian.PutUint64(b, v)
	return b
}

// initDatabases opens required databases and creates missing buckets.
func initDatabases() error {
	var handle *bolt.DB
	var err error

	// Attempt to open configuration database
	if handle, err = bolt.Open(path.Join(*app.DatabasePath, "config.db"), 0660, nil); err != nil {
		return err
	}
	app.ConfigDB = handle

	// Attempt to open database for storing measurement values, events..
	if handle, err = bolt.Open(path.Join(*app.DatabasePath, "timeseries.db"), 0660, nil); err != nil {
		return err
	}
	app.DataDB = handle

	// Check buckets for config
	return app.ConfigDB.Update(func(tx *bolt.Tx) error {
		if _, err := tx.CreateBucketIfNotExists(BucketConfigDevices); err != nil {
			return err
		}

		if _, err := tx.CreateBucketIfNotExists(BucketConfigSubs); err != nil {
			return err
		}

		return nil
	})
}

// loadDevices inits runtime device config from configuration database
func loadDevices() error {
	return app.ConfigDB.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(BucketConfigDevices)

		return b.ForEach(func(k []byte, v []byte) error {
			// Unmarshal the data
			var protoDev BaseDevice
			if err := json.Unmarshal(v, &protoDev); err != nil {
				return err
			}

			// init map for the senderID if not set yet.
			devmap, ok := app.DeviceMap[protoDev.GetSenderID()]
			if !ok {
				devmap = []Device{}
			}

			// Check the device type and initialize correct subtype
			switch protoDev.GetType() {
			case "temperature":
				dev := &Temperature{}
				if err := dev.FromJSON(v); err != nil {
					return err
				}

				devmap = append(devmap, dev)
				break

			case "humidity":
				dev := &Humidity{}
				if err := dev.FromJSON(v); err != nil {
					return err
				}

				devmap = append(devmap, dev)
				break

			case "button":
				dev := &ButtonPanel{}
				if err := dev.FromJSON(v); err != nil {
					return err
				}

				devmap = append(devmap, dev)
				break
			case "contact":
				dev := &Contact{}
				if err := dev.FromJSON(v); err != nil {
					return err
				}

				devmap = append(devmap, dev)
				break
			case "outlet":
				dev := &Outlet{}
				if err := dev.FromJSON(v); err != nil {
					return err
				}

				devmap = append(devmap, dev)
				break
			case "illumination":
				dev := &Illumination{}
				if err := dev.FromJSON(v); err != nil {
					return err
				}

				devmap = append(devmap, dev)
				break
			}

			app.DeviceMap[protoDev.GetSenderID()] = devmap

			// Initialize measurement buckets
			return app.DataDB.Update(func(tx *bolt.Tx) error {
				bName := itob(protoDev.GetGID())
				_, err := tx.CreateBucketIfNotExists(bName)
				return err
			})
		})
	})
}

// storeDevices saves current device state to config database.
func storeDevices() error {
	return app.ConfigDB.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(BucketConfigDevices)

		// Iterates runtime map of devices and store.
		for _, dmap := range app.DeviceMap {
			for _, d := range dmap {
				// Generate a GID if item doesn't have one yet.
				if d.GetGID() == 0 {
					log.Printf("device %d has no GID (%d), generating...", d.GetSenderID(), d.GetGID())
					gid, _ := b.NextSequence()
					d.SetGID(gid)
				}

				buf, err := json.Marshal(d)
				if err != nil {
					return err
				}

				// Persist bytes to users bucket.
				if err := b.Put(itob(d.GetGID()), buf); err != nil {
					return err
				}
			}
		}
		return nil
	})
}

// stores given device value to timeseries database.
func storeDeviceValue(timestamp time.Time, d Device) error {
	return app.DataDB.Update(func(tx *bolt.Tx) error {
		bName := itob(d.GetGID())

		b, err := tx.CreateBucketIfNotExists(bName)
		if err != nil {
			return err
		}

		val := d.GetValue()
		key := []byte(timestamp.UTC().Format(time.RFC3339))
		val.Timestamp = timestamp
		bval, err := json.Marshal(val)
		if err != nil {
			return err
		}

		return b.Put(key, bval)
	})
}

// data pusher
func deviceDataPusher() {
	ticker := time.Tick(1 * time.Minute)

	for {
		select {
		case <-ticker:
			timestamp := time.Now()
			for mk := range app.DeviceMap {
				for dk := range app.DeviceMap[mk] {
					if err := storeDeviceValue(timestamp, app.DeviceMap[mk][dk]); err != nil {
						log.Printf("error storing value: %s", err)
					}
				}
			}
		}
	}
}

// loadSubscriptions inits runtime subscription config from configuration db
func loadSubscriptions() error {

	return nil
}
