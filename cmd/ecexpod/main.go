package main

import (
	"flag"
	"io"
	"log"
	"net/http"

	"github.com/boltdb/bolt"
	"github.com/huin/goserial"
)

type appConfig struct {
	ServerAddr   *string
	FrontendPath *string
	DatabasePath *string
	DevicePath   *string
	DeviceBaud   *int

	ConfigDB      *bolt.DB
	DataDB        *bolt.DB
	DeviceMap     map[uint32][]Device
	Serial        io.ReadWriteCloser
	SubscriberMap map[string]Subscriber
}

// Application configuration, as read from flags etc.
var app = appConfig{}

// Init is run before main, we'll handle command-line etc here.
func init() {
	app.ServerAddr = flag.String("addr", ":8000", "Server listen addr:port pair")
	app.DatabasePath = flag.String("db", "/srv/enoservice/database", "Path to database files.")
	app.DevicePath = flag.String("device", "/dev/ttyUSB0", "EnOcean serial port device path")
	app.DeviceBaud = flag.Int("baudrate", 57600, "Serial device baudrate")
	app.FrontendPath = flag.String("frontend", "/srv/enoservice/frontend", "Path to frontend static resources")

	// Parse flags.
	flag.Parse()

	// Init runtime config maps
	app.DeviceMap = make(map[uint32][]Device)
	app.SubscriberMap = make(map[string]Subscriber)
}

func main() {
	// Initialize databases
	log.Println("Opening databases")
	if err := initDatabases(); err != nil {
		log.Fatalf("Error initializing databases: %s", err)
	}

	// Load device configuration
	log.Println("Loading device configuration")
	if err := loadDevices(); err != nil {
		log.Fatalf("Error loading device configuration: %s", err)
	}

	// Load subscribers
	log.Println("Loading subscriptions")
	if err := loadSubscriptions(); err != nil {
		log.Fatalf("Error loading subscriptions: %s", err)
	}

	log.Printf("Attempting to open serial device %s @ %d\n", *app.DevicePath, app.DeviceBaud)
	d := &goserial.Config{Name: *app.DevicePath, Baud: *app.DeviceBaud}
	s, err := goserial.OpenPort(d)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Starting to EEP traffic routines")
	go handleESP3(s)

	log.Println("Starting measurement saving")
	go deviceDataPusher()

	// Init router
	router := setupRoutes()

	log.Printf("Starting HTTP service in %s\n", *app.ServerAddr)
	log.Fatalf("Failed to start HTTP service: %s", http.ListenAndServe(*app.ServerAddr, router))
}
