package main

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"time"
)

// emit transmits the given device state to all interested subscribers.
func emit(d Device) {
	needle := d.GetSenderID()

	// Run through the subscriber map and transmit states to those who want it.
	for _, sub := range app.SubscriberMap {
		if sub.Search(needle) == true {
			go emitTransmit(sub.CallbackURL, d)
		}
	}
}

// emitTransmit is called as a go-routine to do emits asynchronously.
func emitTransmit(url string, d Device) {
	b, err := json.Marshal(d)
	if err != nil {
		log.Printf("Error emitting: %s\n", err)
		return
	}

	log.Printf("Emitting to %s: `%s`", url, b)

	body := bytes.NewBuffer(b)
	client := http.Client{Timeout: time.Duration(30 * time.Second)}
	resp, err := client.Post(url, "application/json", body)
	if err != nil {
		log.Printf("Error emitting: %s\n", err)
		return
	}

	defer resp.Body.Close()
	log.Printf("Emit done to `%s`", url)
}
