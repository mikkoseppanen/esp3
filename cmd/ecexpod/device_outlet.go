package main

import (
	"encoding/json"
	"log"
	"time"

	"bitbucket.org/mikkoseppanen/esp3"
)

// Outlet defines fields for a Outlet sensor that extends the base
// device type
type Outlet struct {
	BaseDevice
	OverCurrent  bool `json:"overcurrent"`
	PowerFailure bool `json:"powerfailure"`
	LocalControl bool `json:"localcontrol"`
	ErrorLevel   int  `json:"condition"`
	OutputValue  int  `json:"value"`
}

// ToJSON returns the iten as a JSON serialized version.
func (d *Outlet) ToJSON() ([]byte, error) {
	return json.MarshalIndent(d, "", "  ")
}

// FromJSON initializes the state of the device from JSON
func (d *Outlet) FromJSON(data []byte) error {
	return json.Unmarshal(data, d)
}

// Parse parses the input radio packet
func (d *Outlet) Parse(rp *esp3.RadioPacket) error {
	switch d.EEP {
	case "D2-01-09":
		value, err := esp3.EEPParseD20109(rp.Data)
		if err != nil {
			log.Printf("[0x%08x %s] Invalid data: %v: %s\n", d.SenderID, d.EEP, rp.Data, err)
			return err
		}

		log.Printf("[0x%08x %s] %+v %s\n", d.SenderID, d.EEP, value, d.Unit)

		d.OutputValue = value.OutputValue
		d.OverCurrent = value.OverCurrentSwitchActive
		d.PowerFailure = value.PowerFailureSupported && value.PowerFailureDetected
		d.LocalControl = value.LocalControl
		d.ErrorLevel = value.ErrorLevel & 0x3
		d.LastSeen = time.Now()
		d.RSSI = int(rp.RSSI)

		break
	}

	return nil
}

// GetValue returns value of the device.
func (d *Outlet) GetValue() DeviceValue {
	return DeviceValue{Timestamp: d.LastSeen, Value: float64(d.OutputValue)}
}

// NewOutlet returns an initialized Outlet struct with given defaults
func NewOutlet(sid uint32, eep string, name string) *Outlet {
	return &Outlet{
		BaseDevice: BaseDevice{
			SenderID: sid,
			EEP:      eep,
			Type:     "outlet",
			Unit:     "power%",
			Name:     name,
		},
	}
}
