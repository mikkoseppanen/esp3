package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
)

// HandleSubscriptionCreate handles a request for adding a new subscription.
func HandleSubscriptionCreate(w http.ResponseWriter, r *http.Request) {
	sub := &Subscriber{}

	// Read request body
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := json.Unmarshal(body, sub); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if err := sub.Validate(); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Sort subscriptions
	sub.Sort()

	// Assign a key for the new subscription and make sure it's a new key.
	for {
		sub.GenerateKey()
		if _, ok := app.SubscriberMap[sub.Key]; ok == false {
			break
		}
	}

	// Add to global collection
	app.SubscriberMap[sub.Key] = *sub

	w.Header().Add("Location", "/api/subscription/"+sub.Key)
	w.WriteHeader(http.StatusCreated)
}

// HandleSubscriptionDelete handles a request for unsubscribing
func HandleSubscriptionDelete(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	_, ok := app.SubscriberMap[vars["key"]]
	if !ok {
		http.NotFound(w, r)
		return
	}

	// delete from map
	delete(app.SubscriberMap, vars["key"])
}

// HandleSubscriptionRead returns details of a single subscription
func HandleSubscriptionRead(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	sub, ok := app.SubscriberMap[vars["key"]]
	if !ok {
		http.NotFound(w, r)
		return
	}

	out, err := json.MarshalIndent(sub, "", "  ")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Write(out)
}

// HandleSubscriptionReadAll returns details of all current subscriptions
func HandleSubscriptionReadAll(w http.ResponseWriter, r *http.Request) {
	list := []Subscriber{}

	for _, s := range app.SubscriberMap {
		list = append(list, s)
	}

	out, err := json.MarshalIndent(list, "", "  ")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Write(out)
}
