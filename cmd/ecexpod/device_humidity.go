package main

import (
	"encoding/json"
	"log"
	"time"

	"bitbucket.org/mikkoseppanen/esp3"
)

// Humidity defines fields for a Humidity sensor that extends the base
// device type
type Humidity struct {
	BaseDevice
	Value *float64 `json:"value"`
}

// ToJSON returns the iten as a JSON serialized version.
func (d *Humidity) ToJSON() ([]byte, error) {
	return json.MarshalIndent(d, "", "  ")
}

// FromJSON initializes the state of the device from JSON
func (d *Humidity) FromJSON(data []byte) error {
	return json.Unmarshal(data, d)
}

// Parse parses the input radio packet
func (d *Humidity) Parse(rp *esp3.RadioPacket) error {
	switch d.EEP {
	case "A5-04-01":
		value, err := esp3.EEPParseA50401(rp.Data)
		if err != nil {
			log.Printf("[0x%08x %s] Invalid data: %v\n", d.SenderID, d.EEP, rp.Data)
			return err
		}

		log.Printf("[0x%08x %s] %v %s (learn: %v)\n", d.SenderID, d.EEP, value.Humidity, d.Unit, value.Learn)

		// Don't update value from learn packets, they could be bogus.
		if value.Learn == false {
			d.Value = &value.Humidity
		}

		d.LastSeen = time.Now()
		d.RSSI = int(rp.RSSI)
		break

	case "A5-04-02":
		value, err := esp3.EEPParseA50402(rp.Data)
		if err != nil {
			log.Printf("[0x%08x %s] Invalid data: %v\n", d.SenderID, d.EEP, rp.Data)
			return err
		}

		log.Printf("[0x%08x %s] %v %s (learn: %v)\n", d.SenderID, d.EEP, value.Humidity, d.Unit, value.Learn)

		// Don't update value from learn packets, they could be bogus.
		if value.Learn == false {
			d.Value = &value.Humidity
		}

		d.LastSeen = time.Now()
		d.RSSI = int(rp.RSSI)
		break
	}

	return nil
}

// GetValue returns value of the device.
func (d *Humidity) GetValue() DeviceValue {
	r := DeviceValue{Timestamp: d.LastSeen, Value: ""}
	if d.Value != nil {
		r.Value = *d.Value
	}

	return r
}

// NewHumidity returns an initialized Humidity struct with given defaults
func NewHumidity(sid uint32, eep string, name string, initialValue *float64) *Humidity {
	return &Humidity{
		BaseDevice: BaseDevice{
			SenderID: sid,
			EEP:      eep,
			Type:     "humidity",
			Unit:     "rh%",
			Name:     name,
		},
		Value: initialValue,
	}
}
