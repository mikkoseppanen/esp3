module.exports = function(grunt) {
  grunt.initConfig({
    clean: {
      dist: {
        src: ['dist/', 'tmp']
      }
    },
    jshint: {
      dist: ['gruntfile.js', 'js/**/*.js', 'test/**/*.js']
    },
    modernizr: {
      dist: {
        'crawl': false,
        'customTests': [],
        'dest': 'tmp/modernizr/modernizr.js',
        'tests': [
          'cookies',
          'svg',
          'flexbox',
          'flexboxlegacy',
          'flexboxtweener',
          'flexwrap',
          'fontface',
          'mediaqueries',
          'csstransitions',
          'localstorage',
          'svgasimg',
          'datauri'
        ],
        'options': [
          'setClasses'
        ],
        'uglify': true
      }
    },
    concat: {
      dist: {
        src: ['js/**/*.js', 'tmp/templates/templates.js'],
        dest: 'tmp/out/js/app.min.js'
      },
      libs: {
        src: [
          'node_modules/es5-shim/es5-shim.min.js',
          'node_modules/html5shiv/dist/html5shiv.min.js',
          'tmp/modernizr/modernizr.js',
          'node_modules/angular/angular.min.js',
          'node_modules/angular-route/angular-route.min.js',
          'node_modules/angular-animate/angular-animate.min.js',
          'node_modules/angular-aria/angular-aria.min.js',
          'node_modules/angular-messages/angular-messages.min.js',
          'node_modules/angular-resource/angular-resource.min.js',
          'node_modules/angular-material/angular-material.min.js',
          'node_modules/d3/d3.min.js',
          'node_modules/nvd3/build/nv.d3.min.js',
          'node_modules/angular-nvd3/dist/angular-nvd3.min.js'
        ],
        dest: 'tmp/out/js/vendor.js'
      },
      css: {
        src: [
          'node_modules/angular-material/angular-material.min.css',
          'node_modules/nvd3/build/nv.d3.min.css',
          'tmp/out/css/inline.min.css'
        ],
        dest: 'tmp/out/css/inline.min.css'
      }
    },
    ngAnnotate: {
      options: {
        singleQuotes: true
      },
      dist: {
        files: {
          'tmp/out/js/app.min.js': 'tmp/out/js/app.min.js'
        }
      }
    },
    ngtemplates: {
      graphApp: {
        src: 'partials/**/*.html',
        dest: 'tmp/templates/templates.js',
        options: {
          htmlmin:  {
            collapseWhitespace: true,
            collapseBooleanAttributes: true,
            keepClosingSlash: true
          }
        }
      }
    },
    uglify: {
      dist: {
        options: {
          preserveComments: 'some'
        },
        files: {
          'tmp/out/js/app.min.js': ['tmp/out/js/app.min.js']
        }
      }
    },
    less: {
      dev: {
        options: {
          compress: false,
          paths: ['assets/less/import'],
          plugins: [
            new (require('less-plugin-autoprefix'))({
              browsers: ['last 2 versions']
            })
          ]
        },
        files: {
          'tmp/out/css/index.min.css': 'assets/less/index.less',
          'tmp/out/css/inline.min.css': [
            'assets/less/inline.less'
          ]
        }
      },
      dist: {
        options: {
          compress: true,
          paths: ['assets/less/import'],
          plugins: [
            new (require('less-plugin-autoprefix'))({
              browsers: ['last 2 versions']
            }),
            new (require('less-plugin-clean-css'))({
              removeSpecialComments: false
            })
          ],
          modifyVars: {
            //imgPath: 'http://mycdn.com/path/to/images'
          }
        },
        files: {
          // In dist, we pack normalize into the inline styles for fast load.
          'tmp/out/css/index.min.css': 'assets/less/index.less',
          'tmp/out/css/inline.min.css': [
            'assets/less/inline.less'
          ]
        }
      }
    },
    inline: {
      dist: {
        src: 'html/*.html',
        dest: 'tmp/'
      }
    },
    favicons: {
      options: {
        appleTouchBackgroundColor: '#ffffff',
        tileBlackWhite: false,
        tileBackgroundColor: '#ffffff',
        HTMLPrefix: '/images/favicons/'
      },
      icons: {
        src: 'assets/favicon/favicon.svg',
        dest: 'dist/images/favicons/'
      }
    },
    copy: {
      html: {
        expand: true,
        flatten: true,
        src: 'tmp/html/**/*.html',
        dest: 'dist/'
      },
      js: {
        expand: true,
        cwd: 'tmp/out',
        src: 'js/**/*.js',
        dest: 'dist/'
      },
      css: {
        expand: true,
        cwd: 'tmp/out',
        src: 'css/**/*.css',
        dest: 'dist/'
      },
      images: {
        expand: true,
        cwd: 'assets/',
        src: 'images/**',
        dest: 'dist/'
      },
      mockdata: {
        expand: true,
        cwd: 'assets/mockdata',
        src: '**/*.json',
        dest: 'dist/api'
      }
    },
    karma: {
      dist: {
        configFile: 'karma.conf.js'
      }
    },
    connect: {
      dist: {
        options: {
          port: 9001,
          base: 'dist/'
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-favicons');
  grunt.loadNpmTasks('grunt-inline');
  grunt.loadNpmTasks('grunt-ng-annotate');
  grunt.loadNpmTasks('grunt-angular-templates');
  grunt.loadNpmTasks('grunt-modernizr');
  grunt.loadNpmTasks('grunt-karma');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.registerTask('dist', [
    'clean',
    'less:dist',
    'jshint',
    'modernizr',
    'ngtemplates',
    'concat',
    'ngAnnotate',
    'uglify',
    'inline',
    'copy:html',
    'copy:css',
    'copy:js',
    'copy:images',
    'favicons'
  ]);
  grunt.registerTask('dev', [
    'clean',
    'less:dev',
    'jshint',
    'modernizr',
    'ngtemplates',
    'concat',
    'ngAnnotate',
    'inline',
    'copy:html',
    'copy:css',
    'copy:js',
    'copy:images'
  ]);
  grunt.registerTask('genfavicons', [
    'favicons'
  ]);
  grunt.registerTask('run', [
    'connect:dist:keepalive'
  ]);
  grunt.registerTask('test', [
    'dist',
    'karma'
  ]);
};
