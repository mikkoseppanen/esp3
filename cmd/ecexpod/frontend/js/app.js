(function(ng, Modernizr) {
  'use strict';

  var app = ng.module('graphApp', [
    'ngMaterial',
    'ngMessages',
    'ngAnimate',
    'ngResource',
    'ngAria',
    'ngRoute',
    'nvd3'
  ]);

  app.constant('Modernizr', Modernizr);

  // Define routes inside the application. Routes determine which views
  // and controllers to invoke from different URLs.
  app.config(['$routeProvider',
    function($routeProvider) {
      // Default route, when nothing else matches.
      $routeProvider.otherwise({
        templateUrl: 'partials/view/graph.html',
        controller: 'GraphCtrl'
      });
    }
  ]);

  app.config(function($mdThemingProvider) {
    // Configure a dark theme with primary foreground yellow
    $mdThemingProvider.theme('default');
  });

})(angular, Modernizr);
