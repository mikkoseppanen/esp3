/* global angular */
(function(ng) {
  'use strict';

  // Register our application.
  ng.module('graphApp').controller('BootCtrl',
    function($scope, $rootScope, Modernizr) {
      'ngInject';

      $scope.state = {
        bootReady: false,
        hasErrors: false,
        notSupported: false,
        noCookies: false,
      };

      $scope.progress = null;

      $scope.state.notSupported = !(Modernizr.svg && Modernizr.flexwrap);
      $scope.state.noCookies = !Modernizr.cookies;
      $scope.state.hasErrors = (
        $scope.state.notSupported ||
        $scope.state.noCookies
      );

      // Listen for main view loaded event. That tells us we're done booting.
      $rootScope.$on('mainview.loaded', function(/*event, data*/) {
        $scope.state.bootReady = true;
      });

      $rootScope.$on('mainview.progress', function(e, data) {
        $scope.progress = data;
      });
    }
  );

})(angular, Modernizr);
