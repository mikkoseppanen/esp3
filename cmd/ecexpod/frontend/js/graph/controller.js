(function(ng, d3) {
  'use strict';

  // Register our application.
  ng.module('graphApp').controller('GraphCtrl',
    function($scope, $rootScope, $filter, $timeout, ecData) {
      'ngInject';

      // Loading status.
      $scope.loader = {
        required: 1,
        loaded: 0
      };

      // start date & time
      $scope.start = {};
      // end date & time
      $scope.end = {};
      // Device options
      $scope.devices = null;
      // Stepping options
      $scope.steppings = [
        {name: '1 minute', value: '1min'},
        {name: '15 minutes', value: '15min'},
        {name: '30 minutes', value: '30min'},
        {name: '1 hour', value: 'hour'}
      ];
      // Selected stepping
      $scope.step = 'hour';
      // Selected source options
      $scope.selectedSources = [];
      // Chart data
      $scope.chart = {
        options: {
          chart: {
            type: 'lineWithFocusChart',
            height: 450,
            margin: {
              top: 20,
              right: 60,
              bottom: 60,
              left: 60
            },
            duration: 500,
            useInteractiveGuideline: true,
            xAxis: {
              axisLabel: 'Time',
              tickFormat: function(d, i) {
                if (i === undefined) {
                  return d3.time.format('%Y-%m-%d %H:%M:%S')(new Date(d));

                } else {
                  return d3.time.format('%H:%M:%S')(new Date(d));
                }
              }
            },
            x2Axis: {
              tickFormat: function(d, i) {
                if (i === undefined) {
                  return d3.time.format('%Y-%m-%d')(new Date(d));

                } else {
                  return d3.time.format('%H:%M:%S')(new Date(d));
                }
              }
            },
            yAxis: {
              axisLabel: 'Value',
              tickFormat: function(d) {
                return d3.format(',.2f')(d);
              },
              rotateYLabel: false
            },
            y2Axis: {
              tickFormat: function(d) {
                return d3.format(',.2f')(d);
              }
            }
          }
        },
        data: []
      };

      // Called when user is finished selecting inputs.
      $scope.loadData = function() {
        var i;
        if ($scope.graphForm.$invalid) {
          return;
        }

        // reset result
        $scope.chart.data = [];

        // Load data for all selected GIDs
        for (i = 0; i < $scope.selectedSources.length; i++) {
          ecData.Measurements.get({
            gid: $scope.selectedSources[i],
            start: $scope.start.date.toISOString(),
            end: $scope.end.date.toISOString(),
            step: $scope.step
          }, parseData);
        }
      };

      // Load data when datetime options change
      var debounceTimer = null;
      $scope.$watchGroup([
        'start.hour', 'end.hour', 'start.minute', 'end.minute'
      ], function() {
        $timeout.cancel(debounceTimer);
        if ($scope.graphForm.$invalid) {
          return;
        }

        debounceTimer = $timeout(function() {
          $scope.start.date.setHours(
            parseInt($scope.start.hour, 10),
            parseInt($scope.start.minute, 10),
            0, 0
          );

          $scope.end.date.setHours(
            parseInt($scope.end.hour, 10),
            parseInt($scope.end.minute, 10),
            0, 0
          );

          $scope.loadData();
        }, 500);
      });

      $scope.$watchGroup(['start.date', 'end.date'], function() {
        if ($scope.graphForm.$invalid) {
          return;
        }

        $scope.start.date.setHours(
          parseInt($scope.start.hour, 10),
          parseInt($scope.start.minute, 10),
          0, 0
        );

        $scope.end.date.setHours(
          parseInt($scope.end.hour, 10),
          parseInt($scope.end.minute, 10),
          0, 0
        );
        
        $scope.loadData();
      });

      // Init routines
      function init() {
        // initialize progress state
        progress({init: true});

        // setup initial dates
        var now = new Date();
        now.setHours(now.getHours(), 0, 0, 0);

        $scope.end = {
          date: new Date(now.getTime()),
          hour: now.getHours(),
          minute: now.getMinutes()
        };

        now = new Date(now.getTime() - (24 * 3600 * 1000));
        now.setHours(now.getHours(), 0, 0, 0);
        $scope.start = {
          date: now,
          hour: now.getHours(),
          minute: now.getMinutes()
        };

        // Load up device listing.
        ecData.Device.query(function(devices) {
          var i;

          var genParser = function(data) {
            parseData(data);
            progress();
          };

          // Pre-select all devices that are suitable for graphing and load
          // data.
          for (i = (devices.length - 1); i >= 0; i--) {
            if (devices[i].unit == 'status') {
              devices.splice(i, 1);

            } else if (
              (devices[i].name.toLowerCase() == 'balcony') &&
              (devices[i].type.toLowerCase() == 'temperature')
            ) {
              $scope.selectedSources.push(devices[i].gid);
              $scope.loader.required += 1;
              ecData.Measurements.get({
                gid: devices[i].gid,
                start: $scope.start.date.toISOString(),
                end: $scope.end.date.toISOString(),
                step: $scope.step
              }, genParser);
            }
          }

          progress();
          $scope.devices = devices;
        });
      }

      // Parses incoming linedata
      function parseData(data) {
        var set = null;

        if (ng.isArray(data.resultset) === false) {
          data.resultset = [];
        }

        set = {
          key: data.name + ' (' + data.type + ' ' + data.unit + ')',
          values: []
        };

        set.values = data.resultset.map(function(d) {
          return {x: new Date(d.timestamp).getTime(), y: d.value};
        });

        $scope.chart.data.push(set);
      }

      // Marks more progress and emits signals
      function progress(cfg) {
        cfg = cfg || {};

        if (cfg.init !== true) {
          $scope.loader.loaded += 1;
        }

        $rootScope.$emit('mainview.progress', $scope.loader);

        if ($scope.loader.loaded === $scope.loader.required) {
          $rootScope.$emit('mainview.loaded');
        }
      }

      // start it all off.
      init();
    }
  );

})(angular, d3);
