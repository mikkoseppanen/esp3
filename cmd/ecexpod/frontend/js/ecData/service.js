/* global angular */
(function(ng) {
  'use strict';

  ng.module('graphApp').factory('ecData',
    function($resource, $q, $http) {
      'ngInject';

      return {
        Device: $resource('/api/device/', null),
        Measurements: $resource('/api/data/:gid', null)
      };
    }
  );
})(angular);
