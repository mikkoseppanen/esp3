package main

import (
	"encoding/json"
	"log"
	"time"

	"bitbucket.org/mikkoseppanen/esp3"
)

// ButtonPanel defines fields for a 4-button button panel that extends the base
// device type
type ButtonPanel struct {
	BaseDevice
	Value []bool `json:"value"`
}

// ToJSON returns the buttons state as a JSON serialized version.
func (d *ButtonPanel) ToJSON() ([]byte, error) {
	return json.MarshalIndent(d, "", "  ")
}

// FromJSON initializes the state of the device from JSON
func (d *ButtonPanel) FromJSON(data []byte) error {
	return json.Unmarshal(data, d)
}

// Parse parses the input radio packet
func (d *ButtonPanel) Parse(rp *esp3.RadioPacket) error {
	switch d.EEP {
	case "F6-02-01":
		value, err := esp3.EEPParseF60201(rp.Data, rp.T21, rp.NU)
		if err != nil {
			log.Printf("[0x%08x %s] Invalid data: %v\n", d.SenderID, d.EEP, rp.Data)
			return err
		}

		log.Printf(
			"[0x%08x %s] { TOP: %v %v / Bottom: %v %v }\n",
			d.SenderID,
			d.EEP,
			value.LeftTop,
			value.RightTop,
			value.LeftBottom,
			value.RightBottom,
		)

		d.Value = []bool{value.LeftTop, value.LeftBottom, value.RightTop, value.RightBottom}
		d.LastSeen = time.Now()
		d.RSSI = int(rp.RSSI)

		break
	}

	return nil
}

// GetValue returns value of the device.
func (d *ButtonPanel) GetValue() DeviceValue {
	return DeviceValue{Timestamp: d.LastSeen, Value: d.Value}
}

// NewButtonPanel returns an initialized ButtonPanel struct with given defaults
func NewButtonPanel(sid uint32, eep string, name string, initialValue []bool) *ButtonPanel {
	return &ButtonPanel{
		BaseDevice: BaseDevice{
			SenderID: sid,
			EEP:      eep,
			Type:     "button",
			Unit:     "status",
			Name:     name,
		},
		Value: initialValue,
	}
}
