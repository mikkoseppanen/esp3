package main

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"reflect"
	"strconv"
	"time"

	"github.com/boltdb/bolt"
	"github.com/gorilla/mux"
)

// DeviceDataResult defines a result set for one devices data
type DeviceDataResult struct {
	GID       uint64        `json:"gid"`
	Name      string        `json:"name"`
	Type      string        `json:"type"`
	Unit      string        `json:"unit"`
	Stepping  string        `json:"step"`
	Min       DeviceValue   `json:"min"`
	Max       DeviceValue   `json:"max"`
	ResultSet []DeviceValue `json:"resultset"`
}

// HandleReadData returns timeseries data for one item identifier by GID
func HandleReadData(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	gid, err := strconv.ParseUint(vars["gid"], 10, 64)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// check that the GID is registered
	var device Device
	for _, d := range app.DeviceMap {
		for i := range d {
			if d[i].GetGID() == gid {
				device = d[i]
				break
			}
		}
	}

	if device == nil {
		http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
		return
	}

	// Locate start & end
	start := time.Time{}
	end := time.Now()
	if s := r.FormValue("start"); s != "" {
		start, err = time.Parse(time.RFC3339Nano, s)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	}

	if s := r.FormValue("end"); s != "" {
		end, err = time.Parse(time.RFC3339Nano, s)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	}

	stepping := 1 * time.Minute
	stepstr := r.FormValue("step")
	if stepstr != "" {
		switch stepstr {
		case "1min":
			stepping = 1 * time.Minute
		case "15min":
			stepping = 15 * time.Minute
		case "30min":
			stepping = 30 * time.Minute
		case "hour":
			stepping = 1 * time.Hour
		default:
			http.Error(w, "Unrecognized step value", http.StatusBadRequest)
			return
		}
	} else {
		stepstr = "1min"
	}

	res := DeviceDataResult{
		GID:       device.GetGID(),
		Name:      device.GetName(),
		Type:      device.GetType(),
		Unit:      device.GetUnit(),
		Stepping:  stepstr,
		ResultSet: []DeviceValue{},
	}

	err = app.DataDB.View(func(tx *bolt.Tx) error {
		bucketName := itob(gid)
		c := tx.Bucket(bucketName).Cursor()

		min := []byte(start.UTC().Format(time.RFC3339))
		max := []byte(end.UTC().Format(time.RFC3339))
		log.Printf("looking between %s - %s", string(min), string(max))

		resval := DeviceValue{}
		curcount := 0
		for k, v := c.Seek(min); k != nil && bytes.Compare(k, max) <= 0; k, v = c.Next() {
			val := DeviceValue{}
			if err := json.Unmarshal(v, &val); err != nil {
				log.Printf("unable to unmarshal data!: %s", err)
				continue
			}

			if reflect.TypeOf(val.Value).Kind() != reflect.Float64 {
				continue
			}

			val.Timestamp = val.Timestamp.Truncate(stepping)

			if resval.Timestamp.IsZero() == true {
				resval = val
				curcount = 1
				continue

			} else if resval.Timestamp.Equal(val.Timestamp) == true {
				resval.Value = (resval.Value.(float64) + val.Value.(float64))
				curcount = curcount + 1
				continue
			}

			resval.Value = (resval.Value.(float64) / float64(curcount))

			if res.Min.Timestamp.IsZero() {
				res.Min = resval

			} else {
				if res.Min.Value.(float64) > resval.Value.(float64) {
					res.Min = resval
				}
			}

			if res.Max.Timestamp.IsZero() {
				res.Max = resval

			} else {
				if res.Max.Value.(float64) < resval.Value.(float64) {
					res.Max = resval
				}
			}

			res.ResultSet = append(res.ResultSet, resval)
			resval = val
			curcount = 1
		}

		if curcount > 1 {
			resval.Value = (resval.Value.(float64) / float64(curcount))

			if res.Min.Timestamp.IsZero() {
				res.Min = resval

			} else {
				if res.Min.Value.(float64) > resval.Value.(float64) {
					res.Min = resval
				}
			}

			if res.Max.Timestamp.IsZero() {
				res.Max = resval

			} else {
				if res.Max.Value.(float64) < resval.Value.(float64) {
					res.Max = resval
				}
			}
		}

		res.ResultSet = append(res.ResultSet, resval)

		return nil
	})

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Output a list of device with the sender id
	b, err := json.MarshalIndent(res, "", "  ")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Write(b)
}
