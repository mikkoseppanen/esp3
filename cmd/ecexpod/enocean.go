package main

import (
	"io"
	"log"

	"bitbucket.org/mikkoseppanen/esp3"
)

// esp3 handling, blocks, so probably should call this as a go-routine.
func handleESP3(serial io.ReadWriteCloser) {
	input := make(chan *esp3.RawPacket)
	errors := make(chan error)
	quit := make(chan int)

	log.Println("Initializing deframer...")
	deframer := esp3.NewDeframer(serial)

	log.Println("Start deframer...")
	go deframer.AsyncReadPacket(input, errors, quit)

	for {
		var packet *esp3.RawPacket
		select {
		case packet = <-input:
			radiopkg, err := packet.AsRadioPacket()
			if err != nil {
				log.Printf("Not a radio pkg: %s\n", err)

			} else {
				dlist, ok := app.DeviceMap[radiopkg.SenderID]
				if !ok {
					log.Printf("Packet from unregistered source: %+v\n", radiopkg)

					continue
				}

				// Parse packet and if no errors were found, we assume we got new data.
				// Thus, we emit when we encounter no errors.
				for _, d := range dlist {
					if err := d.Parse(radiopkg); err == nil {
						emit(d)
					}
				}
			}
			break

		case err := <-errors:
			log.Printf("error: %s\n", err)

		case <-quit:
			log.Fatal("Failure")
		}
	}
}
