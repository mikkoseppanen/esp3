package main

import (
	"errors"
	"log"
	"net/url"
	"sort"

	"bitbucket.org/mikkoseppanen/esp3/hasher"
)

// Subscriber defines configuration for a single subsciption.
type Subscriber struct {
	// Key is the unique identifier of a subscription
	Key string `json:"key,omitempty"`
	// SubscribedTo is a list of Device SenderIDs that the subscriber is interested in
	SubscribedTo []uint32 `json:"subscriptions"`
	// CallbackURL is the URL that is called when one or more of the subscribed
	// devices change state or have otherwise updated data.
	CallbackURL string `json:"callback"`
}

// SubcriptionSlice attaches sort interface methods to uint32 slice
type SubcriptionSlice []uint32

func (p SubcriptionSlice) Len() int {
	return len(p)
}

func (p SubcriptionSlice) Less(i, j int) bool {
	log.Printf("p[%d] %d < p[%d] %d == %v\n", i, p[i], j, p[j], p[i] < p[j])
	return p[i] < p[j]
}

func (p SubcriptionSlice) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

// Validate checks subscriber fields for sanity and returns first found error
func (s *Subscriber) Validate() error {
	if len(s.CallbackURL) == 0 {
		return errors.New("Callback must be defined")
	}

	u, err := url.Parse(s.CallbackURL)
	if err != nil {
		return err
	}

	if u.Scheme != "http" && u.Scheme != "https" {
		return errors.New("Invalid URL scheme (http and https are accepted)")
	}

	if u.IsAbs() == false {
		return errors.New("URL path must be absolute")
	}

	return nil
}

// GenerateKey assigns a key for the subscriber struct
func (s *Subscriber) GenerateKey() {
	s.Key = hasher.GenRandomHashN(20)
}

// Sort sorts subscribed senderids
func (s *Subscriber) Sort() {
	sort.Sort(SubcriptionSlice(s.SubscribedTo))
}

// Search returns if given subscriber is subscribed to given senderID
func (s *Subscriber) Search(sid uint32) bool {
	if sort.IsSorted(SubcriptionSlice(s.SubscribedTo)) == false {
		s.Sort()
	}

	i := sort.Search(len(s.SubscribedTo), func(i int) bool { return s.SubscribedTo[i] >= sid })

	return i < len(s.SubscribedTo) && s.SubscribedTo[i] == sid
}
