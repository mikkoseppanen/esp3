package main

import (
	"time"

	"bitbucket.org/mikkoseppanen/esp3"
)

// DeviceValue is a single value for a device with a timestamp
type DeviceValue struct {
	Timestamp time.Time   `json:"timestamp"`
	Value     interface{} `json:"value"`
}

// Device interface defines the minimal set of methods for any EC device
type Device interface {
	GetGID() uint64
	SetGID(uint64)
	GetSenderID() uint32
	GetEEP() string
	GetType() string
	GetUnit() string
	GetName() string
	GetValue() DeviceValue
	ToJSON() ([]byte, error)
	Parse(*esp3.RadioPacket) error
}

// BaseDevice implements the default fields for EC devices
type BaseDevice struct {
	GID      uint64    `json:"gid"`
	SenderID uint32    `json:"senderid"`
	EEP      string    `json:"eep"`
	Type     string    `json:"type"`
	Unit     string    `json:"unit"`
	Name     string    `json:"name"`
	RSSI     int       `json:"rssi"`
	LastSeen time.Time `json:"lastseen"`
}

// GetGID returns the database GID of the device
func (d *BaseDevice) GetGID() uint64 {
	return d.GID
}

// SetGID returns the database GID of the device
func (d *BaseDevice) SetGID(gid uint64) {
	d.GID = gid
}

// GetSenderID returns the sender ID
func (d *BaseDevice) GetSenderID() uint32 {
	return d.SenderID
}

// GetEEP returns the devices EEP
func (d *BaseDevice) GetEEP() string {
	return d.EEP
}

// GetType returns the abstract type of the device (eg. Temperature)
func (d *BaseDevice) GetType() string {
	return d.Type
}

// GetUnit returns the measurable unit of the device (eg. Celsius)
func (d *BaseDevice) GetUnit() string {
	return d.Unit
}

// GetName returns the device human-readable name
func (d *BaseDevice) GetName() string {
	return d.Name
}

// GetValue returns last known value for the device.
func (d *BaseDevice) GetValue() DeviceValue {
	return DeviceValue{Timestamp: d.LastSeen}
}
