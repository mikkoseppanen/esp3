package main

import (
	"encoding/json"
	"log"
	"time"

	"bitbucket.org/mikkoseppanen/esp3"
)

// Illumination defines fields for a Illumination sensor that extends the base
// device type
type Illumination struct {
	BaseDevice
	Range struct {
		Min float64 `json:"min"`
		Max float64 `json:"max"`
	} `json:"range"`
	SupplyVoltage float64  `json:"supplyvoltage"`
	Value         *float64 `json:"value"`
}

// ToJSON returns the iten as a JSON serialized version.
func (d *Illumination) ToJSON() ([]byte, error) {
	return json.MarshalIndent(d, "", "  ")
}

// FromJSON initializes the state of the device from JSON
func (d *Illumination) FromJSON(data []byte) error {
	return json.Unmarshal(data, d)
}

// Parse parses the input radio packet
func (d *Illumination) Parse(rp *esp3.RadioPacket) error {
	switch d.EEP {
	case "A5-06-01":
		value, err := esp3.EEPParseA50601(rp.Data)
		if err != nil {
			log.Printf("[0x%08x %s] Invalid data: %v\n", d.SenderID, d.EEP, rp.Data)
			return err
		}

		log.Printf("[0x%08x %s] %v %s (learn: %v)\n", d.SenderID, d.EEP, value.Illumination, d.Unit, value.Learn)

		// Don't update value from learn packets, they could be bogus.
		if value.Learn == false {
			d.Value = &value.Illumination
		}

		if value.Range == 0 {
			d.Range.Min = 600
			d.Range.Max = 60000

		} else {
			d.Range.Min = 300
			d.Range.Max = 30000
		}

		d.SupplyVoltage = value.SupplyVoltage
		d.LastSeen = time.Now()
		d.RSSI = int(rp.RSSI)
		break
	}

	return nil
}

// GetValue returns value of the device.
func (d *Illumination) GetValue() DeviceValue {
	r := DeviceValue{Timestamp: d.LastSeen, Value: ""}
	if d.Value != nil {
		r.Value = *d.Value
	}

	return r
}

// NewIllumination returns an initialized Illumination struct with given defaults
func NewIllumination(sid uint32, eep string, name string, initialValue *float64) *Illumination {
	return &Illumination{
		BaseDevice: BaseDevice{
			SenderID: sid,
			EEP:      eep,
			Type:     "illumination",
			Unit:     "lx",
			Name:     name,
		},
		Value: initialValue,
	}
}
