package main

import (
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func dummyreceiver(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	log.Printf("DUMMY: %s\n", body)
}

func setupRoutes() *mux.Router {
	r := mux.NewRouter()

	r.HandleFunc("/api/device", HandleDeviceReadAll).Methods("GET")
	r.HandleFunc("/api/device/{sid:[0-9]+}", HandleDeviceReadOne).Methods("GET")
	r.HandleFunc("/api/data/{gid:[0-9]+}", HandleReadData).Methods("GET")

	r.HandleFunc("/api/subscription", HandleSubscriptionCreate).Methods("POST")
	r.HandleFunc("/api/subscription", HandleSubscriptionReadAll).Methods("GET")
	r.HandleFunc("/api/subscription/{key:[a-zA-Z0-9]+}", HandleSubscriptionRead).Methods("GET")
	r.HandleFunc("/api/subscription/{key:[a-zA-Z0-9]+}", HandleSubscriptionDelete).Methods("DELETE")

	r.HandleFunc("/api/dummy", dummyreceiver).Methods("POST")

	// Add a file server for the frrontend stuff
	s := http.StripPrefix("/", http.FileServer(http.Dir(*app.FrontendPath)))
	r.PathPrefix("/").Handler(s)

	return r
}
