package main

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

// HandleDeviceReadAll returns all configured devices to requester.
func HandleDeviceReadAll(w http.ResponseWriter, r *http.Request) {
	list := []Device{}

	for _, dlist := range app.DeviceMap {
		for _, d := range dlist {
			list = append(list, d)
		}
	}

	b, err := json.MarshalIndent(list, "", "  ")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Write(b)
}

// HandleDeviceReadOne returns configuration of one registered device by
// identified by senderid
func HandleDeviceReadOne(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	sid, err := strconv.ParseUint(vars["sid"], 10, 32)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	d, ok := app.DeviceMap[uint32(sid)]
	if !ok {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	// Output a list of device with the sender id
	b, err := json.MarshalIndent(d, "", "  ")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Write(b)
}
