package main

import (
	"encoding/json"
	"errors"
	"log"
	"time"

	"bitbucket.org/mikkoseppanen/esp3"
)

// Temperature defines fields for a temperature sensor that extends the base
// device type
type Temperature struct {
	BaseDevice
	Value *float64 `json:"value"`
}

// ToJSON returns the iten as a JSON serialized version.
func (d *Temperature) ToJSON() ([]byte, error) {
	return json.MarshalIndent(d, "", "  ")
}

// FromJSON initializes the state of the device from JSON
func (d *Temperature) FromJSON(data []byte) error {
	return json.Unmarshal(data, d)
}

// Parse parses the input radio packet
func (d *Temperature) Parse(rp *esp3.RadioPacket) error {
	switch d.EEP {
	case "A5-02-05":
		value, err := esp3.EEPParseA50205(rp.Data)
		if err != nil {
			log.Printf("[0x%08x %s] Invalid data: %v\n", d.SenderID, d.EEP, rp.Data)
			return err
		}

		log.Printf("[0x%08x %s] %v celsius (learn: %v)\n", d.SenderID, d.EEP, value.Temperature, value.Learn)

		// Don't update value from learn packets, they could be bogus.
		if value.Learn == false {
			d.Value = &value.Temperature
		}

		d.LastSeen = time.Now()
		d.RSSI = int(rp.RSSI)

		break

	case "A5-04-01":
		value, err := esp3.EEPParseA50401(rp.Data)
		if err != nil {
			log.Printf("[0x%08x %s] Invalid data: %v\n", d.SenderID, d.EEP, rp.Data)
			return err
		}

		log.Printf("[0x%08x %s] %v %s (learn: %v)\n", d.SenderID, d.EEP, value.Temperature, d.Unit, value.Learn)

		// Don't update value from learn packets, they could be bogus.
		if value.Learn == false {
			d.Value = &value.Temperature
		}

		d.LastSeen = time.Now()
		d.RSSI = int(rp.RSSI)

		break

	case "A5-04-02":
		value, err := esp3.EEPParseA50402(rp.Data)
		if err != nil {
			log.Printf("[0x%08x %s] Invalid data: %v\n", d.SenderID, d.EEP, rp.Data)
			return err
		}

		log.Printf("[0x%08x %s] %v %s (learn: %v)\n", d.SenderID, d.EEP, value.Temperature, d.Unit, value.Learn)

		// Don't update value from learn packets, they could be bogus.
		if value.Learn == false {
			d.Value = &value.Temperature
		}

		d.LastSeen = time.Now()
		d.RSSI = int(rp.RSSI)

		break

	default:
		return errors.New("Unsupported EEP")
	}

	return nil
}

// GetValue returns value of the device.
func (d *Temperature) GetValue() DeviceValue {
	r := DeviceValue{Timestamp: d.LastSeen, Value: ""}
	if d.Value != nil {
		r.Value = *d.Value
	}

	return r
}

// NewTemperature returns an initialized Temperature struct with given defaults
func NewTemperature(sid uint32, eep string, name string, initialValue *float64) *Temperature {
	return &Temperature{
		BaseDevice: BaseDevice{
			SenderID: sid,
			EEP:      eep,
			Type:     "temperature",
			Unit:     "celsius",
			Name:     name,
		},
		Value: initialValue,
	}
}
