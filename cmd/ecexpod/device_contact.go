package main

import (
	"encoding/json"
	"log"
	"time"

	"bitbucket.org/mikkoseppanen/esp3"
)

const (
	// ContactClosed means contact sensors circuit is closed
	ContactClosed = "closed"
	// ContactOpen means contact sensors circuit is open
	ContactOpen = "open"
)

// Contact defines fields for a single contact sensor
// Used for door or window open/closed status
type Contact struct {
	BaseDevice
	Value string `json:"value"`
}

// ToJSON returns the buttons state as a JSON serialized version.
func (d *Contact) ToJSON() ([]byte, error) {
	return json.MarshalIndent(d, "", "  ")
}

// FromJSON initializes the state of the device from JSON
func (d *Contact) FromJSON(data []byte) error {
	return json.Unmarshal(data, d)
}

// Parse parses the input radio packet
func (d *Contact) Parse(rp *esp3.RadioPacket) error {
	switch d.EEP {
	case "F6-02-01":
		value, err := esp3.EEPParseF60201(rp.Data, rp.T21, rp.NU)
		if err != nil {
			log.Printf("[0x%08x %s] Invalid data: %v\n", d.SenderID, d.EEP, rp.Data)
			return err
		}

		log.Printf(
			"[0x%08x %s] %+v\n",
			d.SenderID,
			d.EEP,
			value,
		)

		if value.R1 == 1 {
			d.Value = ContactClosed

		} else {
			d.Value = ContactOpen
		}

		d.LastSeen = time.Now()
		d.RSSI = int(rp.RSSI)

		break
	}

	return nil
}

// GetValue returns value of the device.
func (d *Contact) GetValue() DeviceValue {
	return DeviceValue{Timestamp: d.LastSeen, Value: d.Value}
}

// NewContact returns an initialized Contact struct with given defaults
func NewContact(sid uint32, eep string, name string, initialValue string) *Contact {
	return &Contact{
		BaseDevice: BaseDevice{
			SenderID: sid,
			EEP:      eep,
			Type:     "contact",
			Unit:     "status",
			Name:     name,
		},
		Value: initialValue,
	}
}
