package esp3

import "io"

// Deframer states, should not be needed outside.
const (
	deframerStateInit = iota
	deframerStateHead
	deframerStateCRCH
	deframerStateData
	deframerStateOptData
	deframerStateCRCD
	deframerStateDone
)

// Deframer is an object that can be used to read the EnOcean ESP3 radio data
// in packets.
type Deframer struct {
	r      io.Reader
	state  int
	packet *RawPacket
	buffer []byte
}

// NewDeframer returns a new Deframer
func NewDeframer(r io.Reader) *Deframer {
	return &Deframer{
		r:     r,
		state: deframerStateInit,
	}
}

// Feed progresses the parses forward, returning packets if a complete packet
// was completed.
func (df *Deframer) Feed(input []byte) []RawPacket {
	var res []RawPacket
	for _, c := range input {
		if c == 0x55 {
			df.state = deframerStateHead
			df.packet = &RawPacket{}
			df.buffer = []byte{}
			continue
		}

		switch df.state {
		case deframerStateHead:
			df.buffer = append(df.buffer, byte(c))
			if len(df.buffer) == 4 {
				df.packet.Header.DataLength = int(df.buffer[0])<<8 | int(df.buffer[1])
				df.packet.Header.OptionalDataLength = int(df.buffer[2])
				df.packet.Header.Type = PacketType(df.buffer[3])
				df.state = deframerStateCRCH
			}
			break

		case deframerStateCRCH:
			df.packet.Header.CRC = int(c)
			// TODO validate crc
			df.state = deframerStateData
			break

		case deframerStateData:
			df.packet.Data = append(df.packet.Data, byte(c))
			if len(df.packet.Data) == df.packet.Header.DataLength {
				df.state = deframerStateOptData
			}
			break

		case deframerStateOptData:
			df.packet.OptionalData = append(df.packet.OptionalData, byte(c))
			if len(df.packet.OptionalData) == df.packet.Header.OptionalDataLength {
				df.state = deframerStateCRCD
			}
			break

		case deframerStateCRCD:
			df.packet.CRC = int(c)
			df.state = deframerStateDone
			res = append(res, *df.packet)
		}
	}

	return res
}

// ReadPacket reads ESP3 data from its reader and returns a list of Packets
// when enough data has been received.
func (df *Deframer) ReadPacket() ([]RawPacket, error) {
	var res []RawPacket

	input := make([]byte, 128)
	n, err := df.r.Read(input)
	if err != nil {
		return res, err
	}

	res = df.Feed(input[:n])
	return res, nil
}

// AsyncReadPacket can be used in a go routine to read data asynchronously.
func (df *Deframer) AsyncReadPacket(out chan *RawPacket, errors chan error, quit chan int) {
	for {
		packets, err := df.ReadPacket()
		if err != nil {
			errors <- err
		}

		for _, p := range packets {
			out <- &p
		}
	}
}
