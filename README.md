# EnOcean Serial Protocol 3 GO utils

esp3 package contains utils for reading and parsing EEP data from ESP3 compatible serial devices.

Also contained is a sample service (cmd/ecexpod) that utilizes the ESP3 package for reading EEP data from an ESP3 device and makes the data available through a REST-API.
