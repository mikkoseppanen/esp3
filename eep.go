package esp3

import (
	"errors"
	"fmt"
	"log"
)

// EEPA50205 defines fields for A5-02-05 message value
type EEPA50205 struct {
	Temperature float64
	Learn       bool
}

// EEPA50401 defines fields for A5-04-01 EEP
type EEPA50401 struct {
	Temperature float64
	Humidity    float64
	HasTemp     bool
	Learn       bool
}

// EEPA50402 defines fields for A5-04-02 EEP
type EEPA50402 struct {
	Temperature float64
	Humidity    float64
	HasTemp     bool
	Learn       bool
}

// EEPA50601 defines EEP A5-06-01
type EEPA50601 struct {
	Illumination  float64
	Range         int
	SupplyVoltage float64
	Learn         bool
}

// EEPF60201 defines fields for F6-02-01
type EEPF60201 struct {
	Release     bool
	LeftTop     bool
	LeftBottom  bool
	RightTop    bool
	RightBottom bool
	Bow         bool
	R1          int
	EB          int
	R2          int
	SA          int
}

// EEPD20109 defines fields for D2-01-09 EEP
type EEPD20109 struct {
	PowerFailureSupported   bool
	PowerFailureDetected    bool
	OverCurrentSwitchActive bool
	ErrorLevel              int
	Channel                 int
	LocalControl            bool
	OutputValue             int
}

// EEPParseA50205 parses input 4BS data according to EEP A5-02-05
func EEPParseA50205(input []byte) (*EEPA50205, error) {
	if len(input) != 4 {
		return nil, errors.New("invalid data")
	}

	log.Printf("%v", input)

	value := &EEPA50205{}
	value.Learn = ((input[3] & (1 << 3)) == 0)

	// If not learning mode, then data mode. Validate further
	if value.Learn == false {
		if input[0] != 0 || input[1] != 0 {
			return nil, errors.New("invalid data")
		}

		// Get temperature
		value.Temperature = 40.0 - ((40.0 / 255.0) * float64(input[2]))
	}

	return value, nil
}

// EEPParseA50401 parses input 4BS data according to EEP A5-04-01
func EEPParseA50401(input []byte) (*EEPA50401, error) {
	log.Printf("A50401: %v", input)
	if len(input) != 4 {
		return nil, errors.New("invalid data")
	}

	value := &EEPA50401{}
	value.Learn = ((input[3] & (1 << 3)) == 0)
	value.HasTemp = ((input[3] & (1 << 1)) != 0)

	// If not a learn message, assume data message
	if value.Learn == false {
		value.Humidity = (100.0 / 250.0) * float64(input[1])

		// Only read temperature if it should be available.
		if value.HasTemp {
			value.Temperature = (40.0 / 250.0) * float64(input[2])
		}
	}

	log.Printf("T: %f, H: %f, Learn: %v, HasTemp: %v",
		value.Temperature, value.Humidity, value.Learn, value.HasTemp,
	)

	return value, nil
}

// EEPParseA50402 parses input 4BS data according to EEP A5-04-02
func EEPParseA50402(input []byte) (*EEPA50402, error) {
	log.Printf("A50402: %v", input)
	if len(input) != 4 {
		return nil, errors.New("invalid data")
	}

	value := &EEPA50402{}
	value.Learn = ((input[3] & (1 << 3)) == 0)
	value.HasTemp = ((input[3] & (1 << 1)) != 0)

	// If not a learn message, assume data message
	if value.Learn == false {
		value.Humidity = (100.0 / 250.0) * float64(input[1])

		// Only read temperature if it should be available.
		if value.HasTemp {
			value.Temperature = -20.0 + ((80.0 / 250.0) * float64(input[2]))
		}
	}

	log.Printf("T: %f, H: %f, Learn: %v, HasTemp: %v",
		value.Temperature, value.Humidity, value.Learn, value.HasTemp,
	)

	return value, nil
}

// EEPParseA50601 parses input 4BS data according to EEP A5-06-01
func EEPParseA50601(input []byte) (*EEPA50601, error) {
	log.Printf("A50601: %v", input)
	if len(input) != 4 {
		return nil, errors.New("invalid data")
	}

	value := &EEPA50601{}
	value.Learn = ((input[3] & (1 << 3)) == 0)
	value.Range = int(input[3] & (1 << 0))

	// If not a learn message, assume data message
	if value.Learn == false {
		value.SupplyVoltage = (5.1 / 255.0) * float64(input[0])

		if value.Range == 0 {
			value.Illumination = 600.0 + ((60000.0 / 255.0) * float64(input[2]))

		} else {
			value.Illumination = 300.0 + ((30000.0 / 255.0) * float64(input[1]))
		}
	}

	log.Printf("V: %f, Range: %d, Lx: %f, Learn: %v",
		value.SupplyVoltage, value.Range, value.Illumination, value.Learn,
	)

	return value, nil
}

// EEPParseF60201 parses input according to EEP F6-02-01
func EEPParseF60201(input []byte, t21 uint8, nu uint8) (*EEPF60201, error) {
	if len(input) != 1 {
		return nil, errors.New("invalid data")
	}

	if t21 != T21PTM2XX {
		return nil, errors.New("Invalid PTM mode, expected PTM2xx")
	}

	rpsbyte := int(input[0])

	value := &EEPF60201{}
	if nu == NUNormal {
		value.SA = rpsbyte & 1
		value.R2 = (rpsbyte >> 1) & 7
		value.EB = (rpsbyte >> 3) & 1
		value.R1 = (rpsbyte >> 4) & 7
		value.LeftTop = (value.R1 == 3)
		value.LeftBottom = (value.R1 == 1)

		if value.SA == 1 {
			value.RightTop = (value.R2 == 3)
			value.RightBottom = (value.R2 == 2)

		} else {
			value.RightTop = (value.R1 == 7)
			value.RightBottom = (value.R1 == 5)
		}

	} else {
		value.EB = (rpsbyte >> 3) & 1
		value.R1 = (rpsbyte >> 4) & 7
	}

	return value, nil
}

// EEPParseD20109 is a parser for D2-01-09 EEP data.
func EEPParseD20109(input []byte) (*EEPD20109, error) {
	if len(input) < 1 {
		return nil, errors.New("invalid data")
	}

	cmd := int(input[0] & 0x0f)
	value := &EEPD20109{}

	switch cmd {
	case 0x04: //Actuator status response
		if len(input) != 3 {
			return nil, errors.New("invalid data")
		}

		value.PowerFailureSupported = (input[0] & 0x80) != 0
		value.PowerFailureDetected = (input[0] & 0x40) != 0
		value.OverCurrentSwitchActive = (input[1] & 0x80) != 0
		value.ErrorLevel = (int(input[1]) & 0x60) >> 5
		value.Channel = int(input[1]) & 0x1f
		value.LocalControl = (input[2] & 0x80) != 0
		value.OutputValue = int(input[2]) & 0x7f

		break

	default:
		return nil, fmt.Errorf("Unsupported VLD cmd: %x\n", cmd)
	}

	return value, nil
}
